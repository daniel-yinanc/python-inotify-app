import argparse
import datetime
import pyinotify

__author__ = 'dyinanc'

# The watch manager stores the watches and provides operations on watches
wm = pyinotify.WatchManager()

mask = pyinotify.ALL_EVENTS

class EventHandler(pyinotify.ProcessEvent):

    def process_IN_CREATE(self, event):
        printXMLOnly("Created:" + event.pathname)

    def process_IN_DELETE(self, event):
        printXMLOnly("Deleted:" + event.pathname)

    def process_IN_ATTRIB(self, event):
        printXMLOnly("Attrib Changed:" + event.pathname)

    def process_IN_DELETE_SELF(self, event):
        printXMLOnly("Self Deleted:" + event.pathname)

    def process_IN_MODIFY(self, event):
        printXMLOnly("Modified:" + event.pathname)

    def process_IN_MOVED_FROM(self, event):
        printXMLOnly("Moved From:" + event.pathname)

    def process_IN_MOVED_TO(self, event):
        printXMLOnly("Moved To:" + event.pathname)

def printXMLOnly(string):
    if ".xml" in string:
        print(string + " on:" + str(datetime.datetime.now()))

# Interpreter runs this if it is ran as an "application"; if methods are being accessed(aka Import)
# ... will not execute this part of the code.
if __name__ == '__main__':
    #Command line argument handling
    parser = argparse.ArgumentParser(description='This is a demo script by Derya Yinanc.')
    parser.add_argument('-p', '--path', help='Path', required=True)

    args = parser.parse_args()
    path = args.path


    handler = EventHandler()
    notifier = pyinotify.Notifier(wm, handler)


    wdd = wm.add_watch(path, mask, rec=True, auto_add=True, proc_fun=handler)

    notifier.loop()
